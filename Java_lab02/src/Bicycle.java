//Rony-Estefan Maheux-Saban 1931517


public class Bicycle {

	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	//default constructor
	public Bicycle() {
		this.manufacturer = "N/A";
		this.numberGears = 0;
		this.maxSpeed = 0.0;
	}
	//parameterized constructor
	public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
		this.manufacturer = manufacturer;
		this.numberGears = numberGears;
		this.maxSpeed = maxSpeed;
	}
	
	//getter functions
	public String getManufacturer() {
		return this.manufacturer;
	}
	
	public int getNumberGears() {
		return this.numberGears;
	}
	
	public double getMaxSpeed() {
		return this.maxSpeed;
	}
	
	@Override
	public String toString() {
		return "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", MaxSpeed: " + this.maxSpeed + ".";
	}
}
